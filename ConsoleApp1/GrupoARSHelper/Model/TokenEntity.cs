﻿using System;
namespace GrupoARSHelper.Model
{
    public class TokenEntity
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public long expires_in { get; set; }
        public string refreshtoken { get; set; }
        public string userName { get; set; }
        public string roleName { get; set; }
        public string userid { get; set; }
        public string @as_client_id { get; set; }
        public string acceptedprivacy { get; set; }
        public string issued { get; set; }
        public string expires { get; set; }
    }

    public class ErrorResponse
    {

        public string  error { get; set; }
        public string error_description { get; set; }
    }
}
