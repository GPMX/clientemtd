﻿using System;
using System.Collections.Generic;

namespace GrupoARSHelper.Model
{
    public class PublicProject
    {
     
        public string Description { get; set; }
        public string WebPage { get; set; }
        public string ListPricePath { get; set; }
        public string Sale { get; set; }
        public string SaleDescription { get; set; }
        public object ImageKey { get; set; }
        public int ID { get; set; }
        public DateTime Created { get; set; }
        public int CreatedBy { get; set; }
        public object CreatedByDescription { get; set; }
        public DateTime Modified { get; set; }
        public int ModifiedBy { get; set; }
        public object ModifiedByDescription { get; set; }
        public int StatusId { get; set; }
        public object Status { get; set; }
        public string Name { get; set; }
    }

    public class Image
    {
        public int PublicProyectId { get; set; }
        public string Path { get; set; }
        public object ImageKey { get; set; }
        public int ID { get; set; }
        public DateTime Created { get; set; }
        public int CreatedBy { get; set; }
        public object CreatedByDescription { get; set; }
        public object Modified { get; set; }
        public object ModifiedBy { get; set; }
        public object ModifiedByDescription { get; set; }
        public int StatusId { get; set; }
        public object Status { get; set; }
        public string Name { get; set; }
    }

    public class Department
    {
        public int PublicProyectId { get; set; }
        public object PublicProyect { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public object ImageKey { get; set; }
        public int ID { get; set; }
        public DateTime Created { get; set; }
        public int CreatedBy { get; set; }
        public object CreatedByDescription { get; set; }
        public object Modified { get; set; }
        public object ModifiedBy { get; set; }
        public object ModifiedByDescription { get; set; }
        public int StatusId { get; set; }
        public object Status { get; set; }
        public string Name { get; set; }
    }

    public class RootObject
    {
        public PublicProject PublicProject { get; set; }
        public List<Image> Images { get; set; }
        public List<Department> Departments { get; set; }
    }
}
