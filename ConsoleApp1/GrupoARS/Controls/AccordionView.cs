﻿using GrupoARS.Helpers;
using GrupoARS.Views.Proyectos;
using GrupoARSHelper.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GrupoARS.Controls
{
    public class DefaultTemplate : AbsoluteLayout
    {
        public DefaultTemplate()
        {


            this.Padding = 5;

            this.HeightRequest = 50;
            var title = new Label { HorizontalTextAlignment = TextAlignment.Start, HorizontalOptions = LayoutOptions.StartAndExpand };
            var price = new Label { HorizontalTextAlignment = TextAlignment.End, HorizontalOptions = LayoutOptions.End };
            var StackImages = new Xamarin.Forms.Image();
            this.Children.Add(title, new Rectangle(0, 0.5, 0.5, 1), AbsoluteLayoutFlags.All);
            this.Children.Add(price, new Rectangle(1, 0.5, 0.5, 1), AbsoluteLayoutFlags.All);

             title.SetBinding(Label.TextProperty, "DescripcionDesarrollo", stringFormat: "{0:dd MMM yyyy}");
            StackImages.SetBinding(Xamarin.Forms.Image.SourceProperty, "ImgDeptoSource");


         }
    }

    public class AccordionView : ScrollView
    {
        private StackLayout _layout = new StackLayout { Spacing = 1, };

        public DataTemplate Template { get; set; }
        public DataTemplate SubTemplate { get; set; }

        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create(
                propertyName: "ItemsSource",
                returnType: typeof(IList),
                declaringType: typeof(AccordionSectionView),
                defaultValue: default(IList),
                propertyChanged: AccordionView.PopulateList);

        public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        static Models.Proyectos _proyectos;
        public AccordionView(DataTemplate itemTemplate, Models.Proyectos proyectos)
        {
            _proyectos = proyectos;
            this.SubTemplate = itemTemplate;
            this.Template = new DataTemplate(() => (object)(new AccordionSectionView(itemTemplate, this, proyectos)));

            this.Content = _layout;
        }

        void PopulateList()
        {
            _layout.Children.Clear();

            foreach (object item in this.ItemsSource)
            {
                var template = (View)this.Template.CreateContent();
                var section = item as Section;
                template.BindingContext = item;
                _layout.Children.Add(template);


            }
        }

        static void PopulateList(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((AccordionView)bindable).PopulateList();
        }
    }

    public class AccordionSectionView : StackLayout
    {
        private bool _isExpanded = false;
        private StackLayout _content = new StackLayout { HeightRequest = 0 };

        private Color _headerColor = Color.FromHex("0067B7");
        private ImageSource _arrowRight = ImageSource.FromFile("ic_keyboard_arrow_right_white_24dp.png");
        private ImageSource _arrowDown = ImageSource.FromFile("ic_keyboard_arrow_down_white_24dp.png");
        private AbsoluteLayout _header = new AbsoluteLayout();
        private Xamarin.Forms.Image _headerIcon = new Xamarin.Forms.Image { VerticalOptions = LayoutOptions.Center };
        private Label _headerTitle = new Label { TextColor = Color.White, VerticalTextAlignment = TextAlignment.Center, HeightRequest = 20 };
        private Xamarin.Forms.Image _ImagesDpto = new Xamarin.Forms.Image();
        private DataTemplate _template;


        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create(
                propertyName: "ItemsSource",
                returnType: typeof(IList),
                declaringType: typeof(AccordionSectionView),
                defaultValue: default(IList),
                propertyChanged: AccordionSectionView.PopulateList);

        public IList ItemsSource
        {
            get { return (IList)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly BindableProperty TitleProperty =
            BindableProperty.Create(
                propertyName: "Title",
                returnType: typeof(string),
                declaringType: typeof(AccordionSectionView),
                propertyChanged: AccordionSectionView.ChangeTitle);

        public static readonly BindableProperty ImageProperty =
        BindableProperty.Create(
            propertyName: "SourceImage",
            returnType: typeof(string),
            declaringType: typeof(AccordionSectionView),
            propertyChanged: AccordionSectionView.ChangeImgSource);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public string SourceImageDpto
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }


        View CreateTab1(Models.Proyectos _proyectos)
        {
            Label label = new Label();
            label.Text = _proyectos.Descripcion;
           
            return new ScrollView
            {
                Content = new StackLayout
                {

                    Children = { label }
                }
            };
        }
        View CreateTab2(Models.Proyectos _proyectos)
        {

            var helper = Helpers.RetrieveData.GetProject(_proyectos.Id);
            var myGrid = new Grid() { RowSpacing = 1, ColumnSpacing = 1 };


            myGrid.HorizontalOptions = LayoutOptions.CenterAndExpand;
            myGrid.VerticalOptions = LayoutOptions.CenterAndExpand;


            myGrid.RowDefinitions.Add(new RowDefinition());
            myGrid.RowDefinitions.Add(new RowDefinition());
            myGrid.ColumnDefinitions.Add(new ColumnDefinition());
            myGrid.ColumnDefinitions.Add(new ColumnDefinition());
            myGrid.ColumnDefinitions.Add(new ColumnDefinition());

            var productIndex = 0;
            for (int rowIndex = 0; rowIndex < 2; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < 3; columnIndex++)
                {
                    if (productIndex >= helper.Departments.Count)
                    {
                        break;
                    }

                    var _deptoSelected = helper.Departments[productIndex];
                    productIndex += 1;

                    var label = new Label
                    {
                        Text = "\n" + _deptoSelected.Name,
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalTextAlignment = TextAlignment.Start,
                        FontSize = 10

                    };
                    myGrid.Children.Add(label, columnIndex, rowIndex);
                    var image = new Xamarin.Forms.Image
                    {

                        WidthRequest = 100,
                        HeightRequest = 100,
                        Source = (_deptoSelected.Path != string.Empty) ?
                        string.Concat(Tools.uriService, _deptoSelected.Path.Replace(@"\", "/")) : "",
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.Center,

                    };
                    var tapGestureRecognizer = new TapGestureRecognizer
                    {
                        CommandParameter = _deptoSelected
                    };

                    image.GestureRecognizers.Add(tapGestureRecognizer);
                    tapGestureRecognizer.Tapped += TapGestureRecognizer_Tapped;
                    myGrid.Children.Add(image, columnIndex, rowIndex);
                }
            }




            return new StackLayout
            {


                Children = {
                    myGrid



                }
            };
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {

            var _parameter = (TappedEventArgs)e;

            var _proyectoInfo = _parameter.Parameter as Department;
            var detailPage = new DetallePropiedad(_proyectoInfo);

            await Navigation.PushModalAsync(detailPage);

        }


        private async void TapGestureRecognizer_Tapped_ListPrice(object sender, EventArgs e)
        {

            var _parameter = (TappedEventArgs)e;
            var _proyectoInfo = _parameter.Parameter as GrupoARS.Models.Proyectos;
            var _infoProyect = Helpers.RetrieveData.GetProject(_proyectoInfo.Id);
            string uri = (_infoProyect.PublicProject.ListPricePath != string.Empty) ?
            string.Concat(Tools.uriService, _infoProyect.PublicProject.ListPricePath.Replace(@"\", "/")) : "";
            var detailPage = new Views.Proyectos.ViewPdf(new Uri(uri));

            // var detailPage = new Views.Proyectos(new Uri(uri));
            await Navigation.PushModalAsync(detailPage);

        }


        View CreateTab3(Models.Proyectos _proyectos)
        {



            Xamarin.Forms.Image image = new Xamarin.Forms.Image
            {

                HeightRequest = 25,
                WidthRequest = 35,
                Source = "https://www.chiquipedia.com/imagenes/imagenes-frases01.jpg"
            };

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                CommandParameter = _proyectos
            };

            image.GestureRecognizers.Add(tapGestureRecognizer);
            tapGestureRecognizer.Tapped += TapGestureRecognizer_Tapped_ListPrice;
            return new StackLayout
            {


                Children = {
                    image



                }
            };
        }

        Models.Proyectos _proyectos;
        public AccordionSectionView(DataTemplate itemTemplate, ScrollView parent, Models.Proyectos proyectos)
        {
            _proyectos = proyectos;


            _template = itemTemplate;
            _headerTitle.BackgroundColor = _headerColor;
            _headerIcon.Source = _arrowRight;
            _header.BackgroundColor = _headerColor;


            _header.Children.Add(_headerIcon, new Rectangle(0, 1, .1, 1), AbsoluteLayoutFlags.All);
            _header.Children.Add(_headerTitle, new Rectangle(1, 1, .9, 1), AbsoluteLayoutFlags.All);

            this.Spacing = 0;

            //(View)

            this.Children.Add(_header);

            //Label firstLabel = new Label
            //{
            //    Text = "Label 1",
            //    HorizontalOptions = LayoutOptions.StartAndExpand,
            //    TextColor = Xamarin.Forms.Color.FromHex("#000000")
            //};





            this.Children.Add(_content);


            _header.GestureRecognizers.Add(
            new TapGestureRecognizer
            {
                Command = new Command(async () =>
                {
                    if (_isExpanded)
                    {
                        _headerIcon.Source = _arrowRight;
                        _content.HeightRequest = 0;
                        _content.IsVisible = false;
                        _isExpanded = false;
                    }
                    else
                    {
                        _headerIcon.Source = _arrowDown;
                        _content.HeightRequest = _content.Children.Count * 200;
                        _content.IsVisible = true;
                        _isExpanded = true;

                        // Scroll top by the current Y position of the section
                        if (parent.Parent is VisualElement)
                        {
                            await parent.ScrollToAsync(0, this.Y, true);
                        }
                    }
                })
            }
        );


        }

        void ChangeTitle()
        {
            _headerTitle.Text = this.Title;
        }

        void ChangeSource()
        {
            _headerTitle.Text = this.Title;
        }

        void PopulateList()
        {
            _content.Children.Clear();
            bool iinsert = false;
            bool iinsert2 = false;

            bool iinsert3 = false;

            int x = 0;
            foreach (object item in this.ItemsSource)
            {
                x += 1;
                var template = (View)_template.CreateContent();
                template.BindingContext = item;


                var section = item as GrupoARS.Controls.InfoAccordion;

                if (section != null)
                {

                    if (section.IdSeccion == 1)
                    {
                        if (iinsert == false)
                            _content.Children.Add(
                            CreateTab1(_proyectos)
                            );


                        break;
                    }
                    if (section.IdSeccion == 2)
                    {
                        if (iinsert2 == false)
                            _content.Children.Add(CreateTab2(_proyectos));


                        iinsert2 = true;
                        break;
                    }

                    if (section.IdSeccion == 3)
                    {
                        if (iinsert3 == false)
                            _content.Children.Add(CreateTab3(_proyectos));


                        iinsert3 = true;

                        break;
                    }
                }

                _content.Children.Add(template);


            }
        }

        static void ChangeTitle(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((AccordionSectionView)bindable).ChangeTitle();
        }

        static void ChangeImgSource(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((AccordionSectionView)bindable).ChangeSource();
        }

        static void PopulateList(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((AccordionSectionView)bindable).PopulateList();
        }
    }
}
