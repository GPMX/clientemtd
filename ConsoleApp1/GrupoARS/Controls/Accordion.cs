﻿using GrupoARS.Helpers;
using GrupoARS.Models;
using GrupoARSHelper.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace GrupoARS.Controls
{
    public class InfoAccordion
    {
        public string DescripcionDesarrollo { get; set; }
        public string ImagenUri { get; set; }
        public int IdSeccion { get; set; }

    }
    public class ImagenGalery
    {
        public Xamarin.Forms.Image SourceFromURL { get; set; }


    }
    public class Section
    {
        public string Title { get; set; }
        public string ImgDeptoSource { get; set; }
        public IEnumerable<InfoAccordion> List { get; set; }
        public int IdSeccion { get; set; }
    }

    public class ViewModel
    {

        public IEnumerable<Section> List { get; set; }
    }

    public class Accordion
    {
        public View AccordionViewPage(Models.Proyectos _selectedProject)
        {
            //this.Title = "Accordion";

            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            List<Proyectos> _proyectos = _proyectos = new List<Proyectos>();
            List<Models.Proyectos> proyectosItems = new List<Proyectos>();

            string _publicProjects = string.Empty;


            _publicProjects = Helpers.HttpCall.GetJsonAllPublicProjects();

            var _proyects = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RootObject>>(_publicProjects, settings);
            var _filterProject = _proyects.Where(p => p.PublicProject.ID == _selectedProject.Id).FirstOrDefault();



            var template = new DataTemplate(typeof(DefaultTemplate));
            var view = new AccordionView(template, _selectedProject);
            view.SetBinding(AccordionView.ItemsSourceProperty, "List");
            view.Template.SetBinding(AccordionSectionView.TitleProperty, "Title");
            view.Template.SetBinding(AccordionSectionView.ItemsSourceProperty, "List");
            view.Template.SetBinding(AccordionSectionView.ImageProperty, "ImgDeptoSource");


            List<InfoAccordion> infoAccordions = new List<InfoAccordion>();
            foreach (var item in _filterProject.Departments)
            {
                infoAccordions.Add(new InfoAccordion()
                {
                    DescripcionDesarrollo = item.Description,
                    ImagenUri =
                    (item.Path != string.Empty) ?
                    string.Concat(Tools.uriService, item.Path.Replace(@"\", "/")) : ""
                     ,
                    IdSeccion = 2

                });
            }
            view.BindingContext =
                new ViewModel
                {
                    List = new List<Section> {
                        new Section
                        {
                            IdSeccion=1,
                            Title = "Descripciòn del desarrollo",
                            List = new List<InfoAccordion> {
                                new InfoAccordion {  IdSeccion=1, DescripcionDesarrollo  =  _filterProject.PublicProject.Description,
                                 }
                            }
                        },
                        new Section
                        {
                             IdSeccion=2,
                            Title = "Tipo de departamento",
                            List = infoAccordions
                        },
                        new Section
                        {
                             IdSeccion=3,
                            Title = "Lista de precios",
                              List = new List<InfoAccordion> {
                                new InfoAccordion {  IdSeccion=3, DescripcionDesarrollo  =  _filterProject.PublicProject.Description,
                                 }

                            }
                        },

                    }
                };
            return view;


        }
    }

    //public class App : Application
    //{
    //    public App()
    //    {
    //        MainPage = new NavigationPage(new AccordionViewPage());
    //    }
    //}
}
