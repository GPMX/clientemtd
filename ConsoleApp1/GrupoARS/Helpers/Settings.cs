﻿using GrupoARS.Models.EntityModel;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace GrupoARS.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private const string SettingsKey1 = "settings_key1";

        private const string SettingsKey2 = "settings_key2";

        private static readonly string SettingsDefault = string.Empty;
        private static readonly string SettingsDefault1 = string.Empty;
        private static readonly string SettingsDefault2 = string.Empty;

        private const string IsLoggedInTokenKey = "isloggedid_key";
      
        private static readonly bool IsLoggedInTokenDefault = false;
        private static readonly LoginMaster IsLoggedDataSession = new LoginMaster();

        #endregion


        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }
        public static string ProjectId
        {
            get
            {
                return AppSettings.GetValueOrDefault("ProjectId", SettingsDefault1);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey1, value);
            }
        }
        public static string SectionId
        {
            get
            {
                return AppSettings.GetValueOrDefault("SectionId", SettingsDefault2);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey2, value);
            }
        }
        public static bool IsLoggedIn
        {
            get { 
                return AppSettings.GetValueOrDefault(IsLoggedInTokenKey, IsLoggedInTokenDefault); 
            
            }
            set { 

                AppSettings.AddOrUpdateValue(IsLoggedInTokenKey, value); 
            
            }
        }

      

    }
}
