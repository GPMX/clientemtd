﻿using GrupoARSHelper.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GrupoARS.Helpers
{


    public static class HttpCall
    {

        public static Tuple<bool, object, string> Login(string username, string password)
        {
            var client = new HttpClient();
            string content = "grant_type=password&Username=" + username + "&Password=" + password + "&Client_id=ng2AuthApp";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            var response = client.PostAsync(Tools.uriService + "Token", new StringContent(content)).Result;

            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var desError = Newtonsoft.Json.JsonConvert.DeserializeObject<ErrorResponse>(response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
                return new Tuple<bool, object, string>(false, (ErrorResponse)desError, response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
            }
            var desToken = Newtonsoft.Json.JsonConvert.DeserializeObject<TokenEntity>(response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
            return new Tuple<bool, object, string>(true, (TokenEntity)desToken, response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
        }
 
        public static bool LogOut()
        {

              bool Retur = false;

            var _getSession = Newtonsoft.Json.JsonConvert.DeserializeObject<TokenEntity>(Settings.GeneralSettings);

            string accessToken = _getSession.access_token;
           
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("APP_VERSION", "1.0.0");
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",accessToken);
            HttpResponseMessage response =   httpClient.PostAsync(Tools.LogOut,null).GetAwaiter().GetResult();
            var result = response.Content.ReadAsStringAsync().Result; ;

            if (result == "true")
            {
                Retur = true;


            }
            else
                Retur = false;

            Settings.IsLoggedIn = false;

            return Retur;
        }


        public static string GetJsonAllPublicProjects()
        {

            string res = String.Empty;
            using (var client = new HttpClient())
            {

                HttpResponseMessage response = client.GetAsync(Tools.uriService + "/api/News/GetAll").GetAwaiter().GetResult();
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                }
            }
            return res;

        }
        public static string GetFiles(int projectId,int sectionId)
        {

            string res = String.Empty;
            using (var client = new HttpClient())
            {

                HttpResponseMessage response = client.GetAsync(Tools.UriFiles + "projectId="+projectId+"&sectionId="+sectionId).GetAwaiter().GetResult();
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                }
            }
            return res;

        }

        public static string GetJsonProjectByIdUser()
        {

            string res = String.Empty;
            using (var client = new HttpClient())
            {

                TokenEntity tokenEntity =Newtonsoft.Json.JsonConvert.DeserializeObject<TokenEntity>(Settings.GeneralSettings) ;
               
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenEntity.access_token);

                HttpResponseMessage response = client.GetAsync(Tools.uriServiceByID +tokenEntity.userid).GetAwaiter().GetResult();
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                }
            }
            return res;

        }

        public static string GetJsonSeccion()
        {

            string res = String.Empty;
            using (var client = new HttpClient())
            {

                TokenEntity tokenEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<TokenEntity>(Settings.GeneralSettings);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenEntity.access_token);

                HttpResponseMessage response = client.GetAsync(Tools.UriSecciones).GetAwaiter().GetResult();
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                }
            }
            return res;

        }
        public static async Task<string> GetJsonFromDataAsync(object requestJson, string url)
        {
            var responseBody = string.Empty;

            using (HttpClient client = new HttpClient())
            {

                using (HttpResponseMessage response =
                await client.PostAsync(url,
                   new StreamContent(new MemoryStream(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(requestJson))))))
                {

                    response.EnsureSuccessStatusCode();
                    responseBody = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                }

                return responseBody;


            }


        }

    }
}

