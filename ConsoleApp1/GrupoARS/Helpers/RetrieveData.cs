﻿using GrupoARS.Models;
using GrupoARSHelper.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace GrupoARS.Helpers
{
    public static class RetrieveData
    {

        public static List<RootObject> GetProject()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            string _publicProjects = string.Empty;


            _publicProjects = Helpers.HttpCall.GetJsonAllPublicProjects();

            return JsonConvert.DeserializeObject<List<RootObject>>(_publicProjects, settings);

        }

        public static List<Files> GetFiles(int projectId, int sectionId)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
 

             string    _files = Helpers.HttpCall.GetFiles(projectId,sectionId);

            return JsonConvert.DeserializeObject<List<Files>>(_files, settings);

        }
        public static List<ProyectosInversion> GetProjectByUser()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            string _ProjectsByUser = string.Empty;

            _ProjectsByUser = Helpers.HttpCall.GetJsonProjectByIdUser();

            var _proyects = JsonConvert.DeserializeObject<List<ProyectosInversion>>(_ProjectsByUser, settings);
            return _proyects;

        }
        public static List<Secciones> GetSecciones()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            string _secciones = Helpers.HttpCall.GetJsonSeccion();

            var _seccionesAll = JsonConvert.DeserializeObject<List<Secciones>>(_secciones, settings);
            return _seccionesAll;

        }
        public static RootObject GetProject(int IdProjet)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            string _publicProjects = string.Empty;

            _publicProjects = Helpers.HttpCall.GetJsonAllPublicProjects();

            var _proyects = JsonConvert.DeserializeObject<List<RootObject>>(_publicProjects, settings);
            return _proyects.Where(p => p.PublicProject.ID == IdProjet).FirstOrDefault();

        }


    }
}
