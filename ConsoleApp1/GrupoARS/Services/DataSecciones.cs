﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrupoARS.Helpers;
using GrupoARS.Models;
using Newtonsoft.Json;

namespace GrupoARS.Services
{
    public class DataSecciones: IDataStore<Secciones>
    {
        List<Secciones> _secciones;

        public DataSecciones()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            _secciones = new List<Secciones>();
            List<Secciones> seccionesItems = new List<Secciones>();

            var _sections = RetrieveData.GetSecciones();

            if (_sections != null && _sections.Count > 0)
            {
                for (int i = 0; i < _sections.Count; i++)
                {
                    seccionesItems.Add(
                    new Models.Secciones
                    {
                        ID= _sections[i].ID,
                        Name=_sections[i].Name,
                        Status=_sections[i].Status,
                        Created=_sections[i].Created,
                        CreatedBy=_sections[i].CreatedBy,
                        CreatedByDescription=_sections[i].CreatedByDescription,
                        Modified=_sections[i].Modified,
                        ModifiedBy=_sections[i].ModifiedBy,
                        ModifiedByDescription=_sections[i].ModifiedByDescription,
                        StatusId=_sections[i].StatusId
                    });

                }

            }
            foreach (var item in seccionesItems)
            {
                _secciones.Add(item);
            }


        }
 

        public async Task<bool> AddItemAsync(Secciones item)
        {
            _secciones.Add(item);

            return await Task.FromResult(true);
        }

        Task<bool> IDataStore<Secciones>.AddItemAsync(Secciones item)
        {
            throw new NotImplementedException();
        }

        Task<bool> IDataStore<Secciones>.UpdateItemAsync(Secciones item)
        {
            throw new NotImplementedException();
        }

        Task<bool> IDataStore<Secciones>.DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        async Task<Secciones> IDataStore<Secciones>.GetItemAsync(string id)
        {
            return await Task.FromResult(_secciones.FirstOrDefault(s => s.ID == Convert.ToInt32(id)));
        }

        async Task<IEnumerable<Secciones>> IDataStore<Secciones>.GetItemsAsync(bool forceRefresh)
        {
            return await Task.FromResult(_secciones);
        }
    }
}
