﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrupoARS.Helpers;
using GrupoARS.Models;
using Newtonsoft.Json;

namespace GrupoARS.Services
{
    public class DataProyectosInversion : IDataStore<ProyectosInversion>
    {
        List<ProyectosInversion> _proyectos;

        public DataProyectosInversion()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            _proyectos = new List<ProyectosInversion>();
            List<ProyectosInversion> proyectosItems = new List<ProyectosInversion>();

            var _proyects = RetrieveData.GetProjectByUser();

            if (_proyects != null && _proyects.Count > 0)
            {
                for (int i = 0; i < _proyects.Count; i++)
                {
                    proyectosItems.Add(
                    new Models.ProyectosInversion
                    {
                        ID = _proyects[i].ID,
                        Name = _proyects[i].Name,
                        Information = _proyects[i].Information,
                        Address = _proyects[i].Address,
                        Status=_proyects[i].Status 
                });

                }

            }
            foreach (var item in proyectosItems)
            {
                _proyectos.Add(item);
            }


        }

        //public async Task<ProyectosInversion> GetItemAsync(string id)
        //{
        //    return await Task.FromResult(_proyectos.FirstOrDefault(s => s.ID == Convert.ToInt32(id)));
        //}
        //public async Task<IEnumerable<ProyectosInversion>> GetItemsAsync(bool forceRefresh = false)
        //{
        //    return await Task.FromResult(_proyectos);
        //}


        public async Task<bool> AddItemAsync(ProyectosInversion item)
        {
            _proyectos.Add(item);

            return await Task.FromResult(true);
        }

        Task<bool> IDataStore<ProyectosInversion>.AddItemAsync(ProyectosInversion item)
        {
            throw new NotImplementedException();
        }

        Task<bool> IDataStore<ProyectosInversion>.UpdateItemAsync(ProyectosInversion item)
        {
            throw new NotImplementedException();
        }

        Task<bool> IDataStore<ProyectosInversion>.DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

      async  Task<ProyectosInversion> IDataStore<ProyectosInversion>.GetItemAsync(string id)
        {
            return await Task.FromResult(_proyectos.FirstOrDefault(s => s.ID == Convert.ToInt32(id)));
        }

      async  Task<IEnumerable<ProyectosInversion>> IDataStore<ProyectosInversion>.GetItemsAsync(bool forceRefresh)
        {
            return await Task.FromResult(_proyectos);
        }
    }
}
