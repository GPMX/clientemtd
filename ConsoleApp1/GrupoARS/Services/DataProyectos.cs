﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrupoARS.Helpers;
using GrupoARS.Models;
using GrupoARSHelper.Model;
using Newtonsoft.Json;

namespace GrupoARS.Services
{
    public class DataProyectos : IDataStore<Proyectos>
        {
            List<Proyectos> _proyectos;

            public DataProyectos()
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                _proyectos = new List<Proyectos>();
                List<Models.Proyectos> proyectosItems = new List<Proyectos>();
                
                var _proyects = RetrieveData.GetProject();

                if (_proyects != null && _proyects.Count > 0)
                {
                    for (int i = 0; i < _proyects.Count; i++)
                    {
                        proyectosItems.Add(
                        new Models.Proyectos
                        {
                            Id = _proyects[i].PublicProject.ID,
                            Descripcion = _proyects[i].PublicProject.Description,
                            UniqueId = Guid.NewGuid(),
                            Name = _proyects[i].PublicProject.Name,
                            UriSource = (_proyects[i].Images.Count > 0) ? string.Concat(Tools.uriService, _proyects[i].Images[0].Path.Replace(@"\", "/")) : ""
                        });

                    }

                }
                foreach (var item in proyectosItems)
                {
                    _proyectos.Add(item);
                }


            }

            public async Task<Proyectos> GetItemAsync(string id)
            {
                return await Task.FromResult(_proyectos.FirstOrDefault(s => s.Id == Convert.ToInt32(id)));
            }
            public async Task<IEnumerable<Proyectos>> GetItemsAsync(bool forceRefresh = false)
            {
                return await Task.FromResult(_proyectos);
            }


            public async Task<bool> AddItemAsync(Proyectos item)
            {
                _proyectos.Add(item);

                return await Task.FromResult(true);
            }

            public async Task<bool> UpdateItemAsync(Proyectos item)
            {
                var _item = _proyectos.Where((Proyectos arg) => arg.Id == item.Id).FirstOrDefault();
                _proyectos.Remove(_item);
                _proyectos.Add(item);

                return await Task.FromResult(true);
            }

            public async Task<bool> DeleteItemAsync(string id)
            {
                var _item = _proyectos.Where((Proyectos arg) => arg.Id == Convert.ToInt32(id)).FirstOrDefault();
                _proyectos.Remove(_item);

                return await Task.FromResult(true);
            }


        }
}
