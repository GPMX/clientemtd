﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrupoARS.Helpers;
using GrupoARS.Models;
using Newtonsoft.Json;

namespace GrupoARS.Services
{
    public class DataFiles : IDataStore<Files>
    {

        List<Files> _files;

        public DataFiles(int projectId, int sectionId)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            _files = new List<Files>();
            List<Files> filesItems = new List<Files>();

            var _filesData = RetrieveData.GetFiles(projectId, sectionId);
            if (_filesData != null && _filesData.Count > 0)
            {
                for (int i = 0; i < _filesData.Count; i++)
                {
                    filesItems.Add(
                    new Models.Files
                    {
                        ID = _filesData[i].ID,
                        Created = _filesData[i].Created,
                        CreatedBy = _filesData[i].CreatedBy,
                        CreatedByDescription = _filesData[i].CreatedByDescription,
                        Modified = _filesData[i].Modified,
                        ModifiedBy = _filesData[i].ModifiedBy,
                        ModifiedByDescription = _filesData[i].ModifiedByDescription,
                        Name = _filesData[i].Name,
                        Status = _filesData[i].Status,
                        StatusId = _filesData[i].StatusId,
                        DocumentType = _filesData[i].DocumentType,
                        DocumentTypeId = _filesData[i].DocumentTypeId,
                        DocumentTypes = _filesData[i].DocumentTypes,
                        Path = string.Concat(Tools.uriService, _filesData[i].Path.Replace(@"\", "/")),
                        Phase = _filesData[i].Phase,
                        PhaseId = _filesData[i].PhaseId,
                        Profile = _filesData[i].Profile,
                        ProfileId = _filesData[i].ProfileId,
                        Project = _filesData[i].Project,
                        ProjectId = _filesData[i].ProjectId,
                        Section = _filesData[i].Section,
                        SectionId = _filesData[i].SectionId,
                        UserAccount = _filesData[i].UserAccount,
                        UserId = _filesData[i].UserId,
                        UserLastModification = _filesData[i].UserLastModification

                    });
                }
            }
            foreach (var item in filesItems)
            {
                _files.Add(item);
            }


        }

        //async Task<Files> GetItemAsync(string id)
        //{
        //    return await Task.FromResult(_files.FirstOrDefault(s => s.ID == Convert.ToInt32(id)));
        //}
        //async Task<IEnumerable<Files>> GetItemsAsync(bool forceRefresh = false)
        //{
        //    return await Task.FromResult(_files);
        //}


        async Task<bool> AddItemAsync(Files item)
        {
            _files.Add(item);

            return await Task.FromResult(true);
        }

        async Task<bool> UpdateItemAsync(Files item)
        {
            var _item = _files.Where((Files arg) => arg.ID == item.ID).FirstOrDefault();
            _files.Remove(_item);
            _files.Add(item);

            return await Task.FromResult(true);
        }

        async Task<bool> DeleteItemAsync(string id)
        {
            var _item = _files.Where((Files arg) => arg.ID == Convert.ToInt32(id)).FirstOrDefault();
            _files.Remove(_item);

            return await Task.FromResult(true);
        }

        Task<bool> IDataStore<Files>.AddItemAsync(Files item)
        {
            throw new NotImplementedException();
        }

        Task<bool> IDataStore<Files>.UpdateItemAsync(Files item)
        {
            throw new NotImplementedException();
        }

        Task<bool> IDataStore<Files>.DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

     async  Task<Files> IDataStore<Files>.GetItemAsync(string id)
        {
               return await Task.FromResult(_files.FirstOrDefault(s => s.ID == Convert.ToInt32(id)));
        }

       async  Task<IEnumerable<Files>> IDataStore<Files>.GetItemsAsync(bool forceRefresh)
        {
            return await Task.FromResult(_files);
        }
    }
}

