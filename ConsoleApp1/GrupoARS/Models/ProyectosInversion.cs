﻿using System;
namespace GrupoARS.Models
{
    public class ProyectosInversion
    {
        public string Address { get; set; }
        public string Information { get; set; }
        public int ID { get; set; }
        public DateTime Created { get; set; }
        public int CreatedBy { get; set; }
        public object CreatedByDescription { get; set; }
        public object Modified { get; set; }
        public object ModifiedBy { get; set; }
        public object ModifiedByDescription { get; set; }
        public int StatusId { get; set; }
        public object Status { get; set; }
        public string Name { get; set; }
    }
}
