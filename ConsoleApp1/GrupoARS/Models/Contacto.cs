﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoARS.Models
{
   public class Contacto
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Comentarios { get; set; }
    }
}
