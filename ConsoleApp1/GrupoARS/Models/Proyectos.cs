﻿using System;
using System.Collections.Generic;
using System.Text;
using XLabs.Forms.Controls;

namespace GrupoARS.Models
{
    public class Proyectos
    {

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Name { get; set; }
        public ImageButton Imagen { get; set; }
        public Guid UniqueId { get; set; }
        public string UriSource { get; set; }
        public Uri ListaDePrecioPDF { get; set;     }
    }
}
