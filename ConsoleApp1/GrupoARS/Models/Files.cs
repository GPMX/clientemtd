﻿using System;
namespace GrupoARS.Models
{
    public class Files
    {
        public int ProfileId { get; set; }
        public object Profile { get; set; }
        public int UserId { get; set; }
        public object UserAccount { get; set; }
        public int ProjectId { get; set; }
        public object Project { get; set; }
        public int SectionId { get; set; }
        public object Section { get; set; }
        public int PhaseId { get; set; }
        public object Phase { get; set; }
        public int DocumentTypeId { get; set; }
        public object DocumentType { get; set; }
        public string UserLastModification { get; set; }
        public string Path { get; set; }
        public object DocumentTypes { get; set; }
        public int ID { get; set; }
        public DateTime Created { get; set; }
        public int CreatedBy { get; set; }
        public object CreatedByDescription { get; set; }
        public object Modified { get; set; }
        public object ModifiedBy { get; set; }
        public object ModifiedByDescription { get; set; }
        public int StatusId { get; set; }
        public object Status { get; set; }
        public string Name { get; set; }
    }
}
