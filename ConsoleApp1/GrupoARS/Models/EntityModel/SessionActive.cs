﻿using System;
namespace GrupoARS.Models.EntityModel
{
    public class SessionActive
    {
        public string UserNameActive { get; set; }
        public int IdUser { get; set; }
    }
}
