﻿namespace GrupoARS.Models.EntityModel
{
    public class ProyectosSlider
    {
        public string ImageUrl { get; set; }
        public string Name { get; set; }
    }
}
