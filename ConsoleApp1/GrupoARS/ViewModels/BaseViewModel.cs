﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using GrupoARS.Models;
using GrupoARS.Services;
using GrupoARS.Helpers;

namespace GrupoARS.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        protected void SetObservableProperty<T>(
            ref T field,
            T value,
            [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;
            field = value;
            OnPropertyChanged(propertyName);
        }
        public IDataStore<Item> DataStore => DependencyService.Get<IDataStore<Item>>() ?? new MockDataStore();
        public IDataStore<Models.Proyectos> DataProyectos => DependencyService.Get<IDataStore<Models.Proyectos>>() ?? new DataProyectos();
        public IDataStore<Models.ProyectosInversion> DataProyectosInversion => DependencyService.Get<IDataStore<Models.ProyectosInversion>>() ?? new DataProyectosInversion();
        public IDataStore<Models.Secciones> DataSecciones => DependencyService.Get<IDataStore<Models.Secciones>>() ?? new DataSecciones();
        public IDataStore<Models.Files> DataFiles => DependencyService.Get<IDataStore<Models.Files>>() ?? new DataFiles(Convert.ToInt32(Settings.ProjectId), 
                                                                                                                        Convert.ToInt32(Settings.SectionId));

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
