﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GrupoARS.ViewModels.Proyectos
{
    public class ProyectosDetailViewModel : BaseViewModel

    {

        public Models.Proyectos Item { get; set; }
        public ProyectosDetailViewModel(Models.Proyectos item = null)
        {
            Title = item?.Descripcion;
            Item = item;
        }
    }

}
