﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using GrupoARS.Services;
using Xamarin.Forms;

namespace GrupoARS.ViewModels.Proyectos
{
    public class SeccionesViewModel : BaseViewModel
    {
        public ObservableCollection<Models.Secciones> _Secciones { get; set; }
        public Command LoadItemsCommand { get; set; }

        public SeccionesViewModel()
        {
            Title = "Secciones";
            _Secciones = new ObservableCollection<Models.Secciones>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadProyectosCmd());

            //MessagingCenter.Subscribe<NewItemPage, Item>(this, "AddItem", async (obj, item) =>
            //{
            //    var _item = item as Item;
            //    Items.Add(_item);
            //    await DataStore.AddItemAsync(_item);
            //});
        }

        async Task ExecuteLoadProyectosCmd()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                _Secciones.Clear();
                var items = await DataSecciones.GetItemsAsync(true);
                foreach (var item in items)
                {
                    _Secciones.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
