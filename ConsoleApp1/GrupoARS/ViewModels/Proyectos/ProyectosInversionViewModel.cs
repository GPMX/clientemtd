﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using GrupoARS.Models;
using Xamarin.Forms;

namespace GrupoARS.ViewModels.Proyectos
{
    public class ProyectosInversionViewModel : BaseViewModel
    {
        public ObservableCollection<ProyectosInversion> Proyectos { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ProyectosInversionViewModel()
        {
            Title = "Browse";
            Proyectos = new ObservableCollection<ProyectosInversion>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadProyectosCmd());

            //MessagingCenter.Subscribe<NewItemPage, Item>(this, "AddItem", async (obj, item) =>
            //{
            //    var _item = item as Item;
            //    Items.Add(_item);
            //    await DataStore.AddItemAsync(_item);
            //});
        }

        async Task ExecuteLoadProyectosCmd()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Proyectos.Clear();
                var items = await DataProyectosInversion.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Proyectos.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }

}