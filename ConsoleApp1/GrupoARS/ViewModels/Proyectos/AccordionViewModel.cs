﻿using GrupoARS.Helpers;
using GrupoARS.Pages;
using GrupoARSHelper.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace GrupoARS.ViewModels.Proyectos
{
    public class AccordionViewModel : BaseViewModel
    {
        public AccordionViewModel(GrupoARS.Models.Proyectos _selectedProject)
        {
            

            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            string _publicProjects = String.Empty;

           
                  _publicProjects = Helpers.HttpCall.GetJsonAllPublicProjects();

            var _proyects = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RootObject>>(_publicProjects, settings);


            var _filterProject = _proyects.Where(p => p.PublicProject.ID == _selectedProject.Id).FirstOrDefault();

             Pages = new List<HomeViewModel>();

            List<HomeViewModel> addList = new List<HomeViewModel>();

            int _numberPicture = 1;
            foreach (var item in _filterProject.Images)
            {

                addList.Add(new HomeViewModel()
                {
                    Title = _numberPicture.ToString(),
                    Background = Color.White,
                    ImageSource = string.Concat(Tools.uriService, item.Path.Replace(@"\", "/"))
                });
                _numberPicture += 1;

            }
            Pages = addList;
              CurrentPage = Pages.First();
        }

        IEnumerable<HomeViewModel> _pages;
        public IEnumerable<HomeViewModel> Pages
        {
            get
            {
                return _pages;
            }
            set
            {
                SetObservableProperty(ref _pages, value);
                CurrentPage = Pages.FirstOrDefault();
            }
        }

        HomeViewModel _currentPage;
        public HomeViewModel CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                SetObservableProperty(ref _currentPage, value);
            }
        }
    }

    public class HomeViewModel : BaseViewModel, ITabProvider
    {
        public HomeViewModel() { }

        public string Title { get; set; }
        public Color Background { get; set; }
        public string ImageSource { get; set; }
    }
}

