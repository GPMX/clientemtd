﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using GrupoARS.Models;
using Xamarin.Forms;

namespace GrupoARS.ViewModels.Proyectos
{
    public class FilesViewModel : BaseViewModel
    {
        public ObservableCollection<Files> FilesInfo { get; set; }
        public Command LoadItemsCommand { get; set; }

        public FilesViewModel()
        {
            Title = "Archivos";
            FilesInfo = new ObservableCollection<Files>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadProyectosCmd());

        }

        async Task ExecuteLoadProyectosCmd()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                FilesInfo.Clear();
                var items = await DataFiles.GetItemsAsync(true);
                foreach (var item in items)
                {
                    FilesInfo.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
 
