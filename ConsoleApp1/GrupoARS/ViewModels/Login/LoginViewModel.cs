﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Plugin.Settings.Abstractions;
using Plugin.Settings;
using GrupoARS.Helpers;
using GrupoARS.Models.EntityModel;
using GrupoARS.Views;
using GrupoARSHelper.Model;

namespace GrupoARS.ViewModels.Login
{
    public class LoginPageViewModel : BaseViewModel
    {
        #region Commands
        public INavigation Navigation { get; set; }
        public ICommand LoginCommand { get; set; }
        #endregion

        #region Properties
        private LoginMaster _user = new LoginMaster();

        public Models.EntityModel.LoginMaster User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }
        #endregion

        public LoginPageViewModel()
        {
            LoginCommand = new Command(Login);
        }
        public async void Login()
        {
            IsBusy = true;
            Title = string.Empty;
            try
            {

                if (User.UserName != null)
                {
                    if (User.Password != null)
                    {
                        var _loginInfo = HttpCall.Login(User.UserName, User.Password);


                        //await Navigation.PushAsync(new Views.Principal.Home());
                        if (_loginInfo.Item1)
                        {
                            Settings.IsLoggedIn = true;
                            Settings.GeneralSettings = _loginInfo.Item3;
                            SessionActive sessionActive = new SessionActive();

                            var _current = (TokenEntity)_loginInfo.Item2;

                            sessionActive.UserNameActive = _current.userName;

                            Application.Current.MainPage = new NavigationPage(new Views.Principal.Home(sessionActive));

                        }
                        if (!_loginInfo.Item1)
                        {
                            var errorMsg = _loginInfo.Item2 as ErrorResponse;
                            Message = errorMsg.error_description;
                        }
                        IsBusy = false;
                    }
                    else
                    {
                        IsBusy = false;
                        Message = "La contraseña es requerido";
                    }

                }
                else
                {
                    IsBusy = false;
                    Message = "El usuario es requerido";
                }

            }
            catch (Exception e)
            {
                IsBusy = false;
                await Application.Current.MainPage.DisplayAlert("Error de conexión", e.Message, "Ok");
            }
        }
    }
}
