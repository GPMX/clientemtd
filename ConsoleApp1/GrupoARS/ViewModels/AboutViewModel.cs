﻿using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace GrupoARS.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "About";

            OpenPrincipalCmd = new Command(() => Device.OpenUri(new Uri("https://xamarin.com/platform")));
        }

        public ICommand OpenPrincipalCmd { get; }
    }
}