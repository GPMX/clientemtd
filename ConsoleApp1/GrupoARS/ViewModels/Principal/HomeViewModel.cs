﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace GrupoARS.ViewModels.Principal
{
    public class HomeViewModel : BaseViewModel
    {

        public HomeViewModel()
        {
            OpenWebCommand = new Command(() =>
           (App.Current.MainPage) = new Views.Proyectos.Proyectos());

          //  await Navigation.PushModalAsync(new NavigationPage(new NewItemPage()));

        }

        public ICommand OpenWebCommand { get; }


    }
}
