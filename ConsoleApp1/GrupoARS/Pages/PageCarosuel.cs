﻿using GrupoARS.ViewModels.Proyectos;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GrupoARS.Pages
{
    

    public class PageCarosuel : ContentView
    {
        public PageCarosuel()
        {
            BackgroundColor = Color.White;

            var image = new Image
            {
                HeightRequest = 100,
                WidthRequest = 100

            };
            
            image.GestureRecognizers.Add(new TapGestureRecognizer(OnTap));
            image.SetBinding(Image.SourceProperty, "ImageSource");
            this.SetBinding(BackgroundColorProperty, "Background");

            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Children = {
                    image
                }
            };
        }

        private void OnTap(View arg1, object arg2)
        {
            throw new NotImplementedException();
        }
    }
    public class SpacingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var items = value as IEnumerable<HomeViewModel>;

            var collection = new ColumnDefinitionCollection();
            foreach (var item in items)
            {
                collection.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            }
            return collection;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
