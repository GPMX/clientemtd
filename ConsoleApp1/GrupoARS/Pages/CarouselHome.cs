﻿using GrupoARS.Controls;
using GrupoARS.Models;
using GrupoARS.ViewModels.Proyectos;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GrupoARS.Pages
{
  
    public class CarouselHome : ContentPage
    {

        StackLayout relativeLayout;

        CarouselLayout.IndicatorStyleEnum _indicatorStyle;

        AccordionViewModel viewModel;

        public CarouselHome(CarouselLayout.IndicatorStyleEnum indicatorStyle, GrupoARS.Models.Proyectos proyectos)
        {
            _indicatorStyle = indicatorStyle;

            viewModel = new AccordionViewModel(proyectos);
            BindingContext = viewModel;

            Title = "Proyectos publicos";


            relativeLayout = new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.StartAndExpand

            };
            var pagesCarousel = CreatePagesCarousel();
            var dots = CreatePagerIndicatorContainer();


            switch (pagesCarousel.IndicatorStyle)
            {
                case CarouselLayout.IndicatorStyleEnum.Dots:
                    relativeLayout.Children.Add(pagesCarousel);
                    relativeLayout.Children.Add(CreateAccordion(proyectos));
                    //relativeLayout.Children.Add(dots,
                    //    Constraint.Constant(0),
                    //    Constraint.RelativeToView(pagesCarousel,
                    //        (parent, sibling) => { return sibling.Height - 18; }),
                    //    Constraint.RelativeToParent(parent => parent.Width),
                    //    Constraint.Constant(18)
                    //);
                    break;

            }


            //---------------
            Label header = new Label
            {
                Text = proyectos.Name,
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.Center
            };



            // Build the page.
            //Title = "ScrollBar Demo";
            Content = new StackLayout
            {
                Children =
                {
                    pagesCarousel,
                    header,
                    CreateAccordion(proyectos)
                }
            };
            //Content = relativeLayout;
        }

        CarouselLayout CreatePagesCarousel()
        {
            var carousel = new CarouselLayout
            {
                HeightRequest = 150,

                //IndicatorStyle = _indicatorStyle,
                ItemTemplate = new DataTemplate(typeof(PageCarosuel))
            };

            carousel.SetBinding(CarouselLayout.ItemsSourceProperty, "Pages");
            carousel.SetBinding(CarouselLayout.SelectedItemProperty, "CurrentPage", BindingMode.TwoWay);

            return carousel;
        }

        View CreatePagerIndicatorContainer()
        {

            return new StackLayout
            {
                Children = { CreatePagerIndicators()




                }
            };
        }
        View CreateAccordion(Proyectos _selectedProject)
        {

            var DetailWithAccordion = new Accordion().AccordionViewPage(_selectedProject);

            return new StackLayout
            {
                Children = { DetailWithAccordion




                }
            };
        }

        View CreatePagerIndicators()
        {
            var pagerIndicator = new GrupoARS.Pages.PagerIndicatorDots() { DotSize = 15, DotColor = Color.Black };
            pagerIndicator.SetBinding(GrupoARS.Pages.PagerIndicatorDots.ItemsSourceProperty, "Pages");
            pagerIndicator.SetBinding(GrupoARS.Pages.PagerIndicatorDots.SelectedItemProperty, "CurrentPage");
            return pagerIndicator;
        }



    }

}
