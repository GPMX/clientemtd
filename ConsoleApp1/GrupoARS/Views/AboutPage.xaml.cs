﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GrupoARS.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        private WebView webView;
        private WebView webView2;
        private WebView webView3;

        public AboutPage()
        {
            InitializeComponent();


            webView = new WebView
            {
                Source = "https://drive.google.com/viewer?embedded=true&widget=&chrome=&url=http://www.uba.ar/download/academicos/carreras/lic-letras.pdf",
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
               

            };
            string html = " <embed src = 'http://www.uba.ar/download/academicos/carreras/lic-letras.pdf' width = '600' height = '500' alt = 'pdf' pluginspage = 'http://www.adobe.com/products/acrobat/readstep2.html' >";
                     webView2 = new WebView
            {
                Source = html,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,


            };
            string dd= "<object data='http://www.inspuratesystems.com/clf/wp-content/uploads/2016/05/pdfjoiner.pdf'></object>";

            webView3 = new WebView
            {
                Source = dd,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,


            };



            // toolbar
            ToolbarItems.Add(new ToolbarItem("Back", null, () =>
            {
                webView.GoBack();
            }));

            Content = new StackLayout
            {
                Children = { webView,webView2, webView3 }
            };




        }



    }
}
