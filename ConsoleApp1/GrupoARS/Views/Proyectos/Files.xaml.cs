﻿using System;
using System.Collections.Generic;
using GrupoARS.ViewModels.Proyectos;
using Xamarin.Forms;

namespace GrupoARS.Views.Proyectos
{
    public partial class Files : ContentPage
    {

        FilesViewModel viewModel;
        public Files()
        {
            InitializeComponent();
            BindingContext = viewModel = new FilesViewModel();

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.FilesInfo.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs row)
        {
            try
            {

                if (IsBusy)
                    return;

                IsBusy = true;
                if (!(row.SelectedItem is Models.Files item))
                    return;


              //  await Navigation.PushAsync(new files);
            }
            catch
            {

            }

            finally
            {
                IsBusy = false;

            }
        }
    }
}
