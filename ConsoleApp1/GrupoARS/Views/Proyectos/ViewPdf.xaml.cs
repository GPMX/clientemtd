﻿using System;
using System.Collections.Generic;
using GrupoARS.ViewModels.Proyectos;
using Xamarin.Forms;

namespace GrupoARS.Views.Proyectos
{
    public partial class ViewPdf : ContentPage
    {
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            throw new NotImplementedException();
        }

        ViewPdfViewModel viewModel;
        private WebView webView;


        public ViewPdf(Uri UrlListaPreciosPDF)
        {
            InitializeComponent();

            if (IsBusy)
                return;

            IsBusy = true;

            try
            {

                BindingContext = viewModel = new ViewPdfViewModel(new Models.ViewPdf()
                { UrlPdfListaPrecio = UrlListaPreciosPDF });

                webView = new WebView
                {
                    Source = "https://drive.google.com/viewer?embedded=true&widget=&chrome=&url=" + UrlListaPreciosPDF.ToString(),
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand


                };



                // toolbar
                ToolbarItems.Add(new ToolbarItem("Back", null, () =>
                {
                    webView.GoBack();
                }));

                Content = new StackLayout
                {
                    Children = { webView }
                };
            }
            catch (Exception ex)
            {

            }
            finally
            {

                IsBusy = false;
            }

        }
    }
}
