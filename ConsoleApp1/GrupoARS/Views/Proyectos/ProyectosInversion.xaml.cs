﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using GrupoARS.ViewModels.Proyectos;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GrupoARS.Views.Proyectos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProyectosInversion : ContentPage
    {
        ProyectosInversionViewModel viewModel;
        public ObservableCollection<ProyectosInversion> Proyectos { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ProyectosInversion()
        {
            InitializeComponent();
            BindingContext = viewModel = new ProyectosInversionViewModel();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Proyectos.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs row)
        {
            try
            {

                if (IsBusy)
                    return;
                
                IsBusy = true;
                if (!(row.SelectedItem is Models.ProyectosInversion item))
                    return;
                
               await Navigation.PushAsync(new ProyectosSecciones());
            }
            catch
            {

            }

            finally
            {
                IsBusy = false;

            }
        }

    }
}
