﻿using System;
using System.Collections.Generic;
using GrupoARS.Helpers;
using GrupoARS.ViewModels.Proyectos;
using Xamarin.Forms;

namespace GrupoARS.Views.Proyectos
{
    public partial class ProyectosSecciones : ContentPage
    {
        SeccionesViewModel viewModel;

        public ProyectosSecciones()
        {
            InitializeComponent();
            BindingContext = viewModel = new SeccionesViewModel();

        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel._Secciones.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs row)
        {
            try
            {

                if (IsBusy)
                    return;

                IsBusy = true;
                if (!(row.SelectedItem is Models.Secciones item))
                    return;


                Settings.SectionId = item.ID.ToString();
                Settings.ProjectId = "1";

                  await Navigation.PushAsync(new Views.Proyectos.Files());
            }
            catch
            {

            }

            finally
            {
                IsBusy = false;

            }
        }

    }
}

