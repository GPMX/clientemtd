﻿using GrupoARS.Controls;
using GrupoARS.Pages;
using GrupoARS.ViewModels.Proyectos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GrupoARS.Views.Proyectos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Proyectos : ContentPage
    {
        ProyectosViewModel viewModel;

        public Proyectos()
        {
            InitializeComponent();


            BindingContext = viewModel = new ProyectosViewModel();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Proyectos.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs row)
        {
            try
            {

                if (IsBusy)
                    return;

                IsBusy = true;
                if (!(row.SelectedItem is Models.Proyectos item))
                    return;


                await Navigation.PushAsync(new CarouselHome(CarouselLayout.IndicatorStyleEnum.Dots, item));
            }
            catch
            {

            }

            finally
            {
                IsBusy = false;

            }
        }

    }


}