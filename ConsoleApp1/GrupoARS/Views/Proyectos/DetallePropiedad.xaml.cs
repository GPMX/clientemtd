﻿using GrupoARS.Helpers;
using GrupoARSHelper.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GrupoARS.Views.Proyectos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallePropiedad : ContentPage
    {
        public DetallePropiedad(Department _infoProyect)
        {
            InitializeComponent();

            this._Nombre.Text = _infoProyect.Name;
            this._Descripcion.Text = _infoProyect.Description;
            this._Imagen.Source = (_infoProyect.Path != string.Empty) ?
                string.Concat(Tools.uriService, _infoProyect.Path.Replace(@"\", "/")) : "";

            _Imagen.HorizontalOptions = LayoutOptions.FillAndExpand;


        }

    }
}