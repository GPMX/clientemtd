﻿using GrupoARS.Controls;
using GrupoARS.ViewModels.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Forms.Controls;

namespace GrupoARS.Views.Proyectos
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProyectosDetail : ContentPage
	{
        ProyectosDetailViewModel viewModel;



        public ProyectosDetail(ProyectosDetailViewModel viewModel)
        {

            
                InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        public ProyectosDetail()
        {
            InitializeComponent();

            var item = new Models.Proyectos
            {
            Descripcion="Esta es una Descripcion"
            };

            viewModel = new ProyectosDetailViewModel(item);
            BindingContext = viewModel;
         
        }


    }
}