﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrupoARS.Helpers;
using GrupoARS.Models.EntityModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GrupoARS.Views.Principal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : ContentPage
    {

        void LogOut_Clicked(object sender, EventArgs e)
        {

            bool _logOut = Helpers.HttpCall.LogOut();

            if (_logOut)
            {

                this.lblCurrentUser.Text = string.Empty;
                this.btnLogOut.IsVisible = false;
                this.btnLogin.IsVisible = true;
            }

        }

        public Home(SessionActive sessionActive)
        {
             
            InitializeComponent();
            if (Settings.IsLoggedIn)
            {

                this.btnLogOut.IsVisible = true;
                this.btnLogin.IsVisible = false;
                this.lblCurrentUser.Text = sessionActive.UserNameActive;

            }
            else
            {

                this.btnLogOut.IsVisible = false;
                this.btnLogin.IsVisible = true;
            }
        }

        public Home()
        {
             InitializeComponent();
            if (Settings.IsLoggedIn)
            {
              
                //this.lblCurrentUser Text = sessionActive.UserNameActive;

                     
                this.btnLogOut.IsVisible = true;
                this.btnLogin.IsVisible = false;
            }
            else
            {

                this.btnLogOut.IsVisible = false;
                this.btnLogin.IsVisible = true;
            }


        }

        async void ProyectosPage_Clicked(object sender, EventArgs e)
        {

            if (Settings.IsLoggedIn)
                await Navigation.PushAsync(new Proyectos.ProyectosInversion());
            else
                await Navigation.PushAsync(new Proyectos.Proyectos());


        }
        async void LoginPage_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Login.Login());


        }

        async void QuienesSomos_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new QuienesSomos());

        }

        async void Contacto_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Contacto());

        }
    }
}